-- phpMyAdmin SQL Dump
-- version 4.1.14.8
-- http://www.phpmyadmin.net
--
-- Servidor: db677176832.db.1and1.com
-- Tiempo de generación: 10-04-2017 a las 02:18:52
-- Versión del servidor: 5.5.54-0+deb7u2-log
-- Versión de PHP: 5.4.45-0+deb7u8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db677176832`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nurun_imagenes`
--

CREATE TABLE IF NOT EXISTS `nurun_imagenes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ruta` varchar(100) NOT NULL,
  `fecha_carga` datetime NOT NULL,
  `activo` int(2) NOT NULL DEFAULT '1',
  `iusuario` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Volcado de datos para la tabla `nurun_imagenes`
--

INSERT INTO `nurun_imagenes` (`id`, `ruta`, `fecha_carga`, `activo`, `iusuario`) VALUES
(1, './assets/gifs/1/sonic-04.gif', '2017-04-09 15:24:26', 1, 1),
(2, './assets/gifs/2/tumblr_oms74iq9Mi1vlr66ho1_540.gif', '2017-04-09 15:29:25', 1, 1),
(3, './assets/gifs/3/tumblr_omcowvAVKj1qh676ro1_500.gif', '2017-04-09 15:29:54', 1, 1),
(4, './assets/gifs/4/tumblr_onclsb7J551spkn9jo1_400.gif', '2017-04-10 00:53:25', 1, 2),
(5, './assets/gifs/5/tumblr_ns106n7BQJ1uajezzo1_500.gif', '2017-04-10 01:10:55', 1, 2),
(6, './assets/gifs/6/NotVortex.gif', '2017-04-10 01:48:31', 1, 2),
(7, './assets/gifs/7/dribbble_fistbump-1.gif', '2017-04-10 02:15:01', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nurun_imagenes_status`
--

CREATE TABLE IF NOT EXISTS `nurun_imagenes_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` int(5) NOT NULL,
  `iimagen` int(11) NOT NULL,
  `iusuario` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `nurun_imagenes_status`
--

INSERT INTO `nurun_imagenes_status` (`id`, `status`, `iimagen`, `iusuario`) VALUES
(1, 1, 2, 1),
(2, 2, 1, 1),
(3, 2, 3, 1),
(5, 1, 5, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nurun_usuarios`
--

CREATE TABLE IF NOT EXISTS `nurun_usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(60) NOT NULL,
  `email` varchar(80) NOT NULL,
  `password` varchar(120) NOT NULL,
  `fecha_registro` datetime NOT NULL,
  `activo` int(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `nurun_usuarios`
--

INSERT INTO `nurun_usuarios` (`id`, `nombre`, `email`, `password`, `fecha_registro`, `activo`) VALUES
(1, 'Toño', 'loop_gam@yahoo.com.mx', 'efe6398127928f1b2e9ef3207fb82663', '2017-04-09 10:04:57', 1),
(2, 'Marco García', 'mag.van.gam@gmail.com', 'efe6398127928f1b2e9ef3207fb82663', '2017-04-10 00:52:51', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
