<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NurunModel extends CI_Model {
	
	public function __construct(){
        parent::__construct();
	}
	
	public function load_slider(){
		
		$sql="Select id,ruta from nurun_imagenes Order By fecha_carga desc Limit 3";
		$query = $this->db->query($sql);
		$data["slider"]=$query->result_array();
		$data["error"]=0;
		return $data;

	}
	
	public function load_all_images(){
		$sql="Select id,ruta from nurun_imagenes Order By fecha_carga desc";
		$query = $this->db->query($sql);
		$data["images"]=$query->result_array();
		$data["error"]=0;
		return $data;
	}
	
	/*Insertar Usuario*/
	public function insert_user($info){
		
		if($this->buscar_usuario($info["email"])){
			$data["error"]=1;
			$data["msg"]="Lo sentimos este mail ya esta en uso, intente con otro";
		}else{
			$sql="Insert into nurun_usuarios set nombre=?,email=?,password=?,fecha_registro=?";
			$query = $this->db->query($sql, array('nombre' => ucwords($info["name"]),'email' =>strtolower($info["email"]),'password'=>md5($info["pwd"]),'fecha_registro' => date("Y-m-d H:i:s")));
			if($query){
				
				$insert_id = $this->db->insert_id();
				$sql="Select id,nombre from nurun_usuarios where id = ? and activo=?";
				$query = $this->db->query($sql, array($insert_id,1));
				
				$rows=$query->row();
				$data["info"]=$rows;
				$data["error"]=0;
				$data["msg"]="¡Listo! Tu registro se completo";
				
			}else{
				$data["error"]=1;
				$data["msg"]="Ocurrio un error al insertar el registro, por favor reportarlo al área correspondiente";
			}
		}
		return $data;	
	}
	
	/*Login User*/
	public function login_user($info){
		$sql="Select id,nombre from nurun_usuarios where email = ? and password=? and activo=?";
		$query = $this->db->query($sql, array(strtolower($info["email"]),'password'=>md5($info["pwd"]),1));
		if($query->num_rows()>0){
			$rows=$query->row();
			$data["info"]=$rows;
			$data["error"]=0;
			$data["msg"]="¡Biemvemido!";
		}else{
			$data["error"]=1;
			$data["msg"]="Email y/o Password incorrectos";
		}
		return $data;
	}
	
	/*Funcion para subir archivos*/
	public function upfile($file){
		
		$allowed =  array('gif');/*Extensiones permitidas*/
		$filename = $file['userImage']['name'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		
		if(!in_array($ext,$allowed) ) {
    		$data["msg"]="El tipo de archivo es incorrecto"; 
			$data["error"]=1;
		}else{
			$sql="Insert into nurun_imagenes set fecha_carga=?,iusuario=?";
			$query = $this->db->query($sql,array('fecha_carga' => date("Y-m-d H:i:s"),'iusuario'=>$this->session->userdata["id"]));
			$insert_id = $this->db->insert_id();
		    $carpeta= "./assets/gifs/".$insert_id;
			if(!is_dir($carpeta)){
				mkdir($carpeta,0777,true);
			}
			$nueva_ruta=$carpeta."/".$filename;
			if(move_uploaded_file($file['userImage']['tmp_name'],$nueva_ruta)) {				
				$data=array('ruta'=>$nueva_ruta,'fecha_carga' => date("Y-m-d H:i:s"));
				$this->db->where('id', $insert_id);
				$this->db->update('nurun_imagenes', $data);		
				$data["msg"]="Archivo guardado correctamente";
				$data["ruta"]=$nueva_ruta;
				$data["id"]=$insert_id;
				$data["error"]=0;
			}else{
				$data["msg"]="No se pudo subir el archivo, intentelo mas tarde";
				$data["error"]=1;
			}
			
		}
		
		return $data;
	}
	
	public function add_status_gif($info){
		$sql="Select id from nurun_imagenes where id = ? and activo=?";
		$query = $this->db->query($sql, array($info["igif"],1));
		if($query->num_rows()>0){
			$sql="Select id from nurun_imagenes_status where iimagen = ? and iusuario=?";
			$query = $this->db->query($sql, array($info["igif"],$this->session->userdata["id"]));
			
			if($query->num_rows()>0){
				$iimagen=$query->row_array();
				#Ya se tiene un registro, procedemos a actualizar el status
				$data=array('status' => $info["accion"]);
				$this->db->where('id',$iimagen["id"]);
				$this->db->update('nurun_imagenes_status', $data);	
				
				$data["msg"]="Listo se ha actualizado el status de tu imagen";
				$data["error"]=0;	
				
			}else{
				#No hay registros, procedemos a insertalo
				$sql="Insert into nurun_imagenes_status set status=?,iimagen=?,iusuario=?";
				$query = $this->db->query($sql, array('status' => $info["accion"],'iimagen' =>$info["igif"],'iusuario'=>$this->session->userdata["id"]));
				if($query){
					$data["msg"]="Listo tu imagen ha cambiado de status";
					$data["error"]=0;
				}else{
					$data["msg"]="Ocurrio un error al insertar el registro, por favor reportarlo al área correspondiente";
					$data["error"]=1;
				}
			}
			
		}else{
			$data["msg"]="Lo sentimos esta imagen ya no se encuentra disponible";
			$data["error"]=1;
		}
		return $data;
	}
	
	public function filter_gifs($info){

		$this->db->select('ruta,nurun_imagenes.id');
		$this->db->from('nurun_imagenes_status');
		$this->db->join('nurun_imagenes', 'nurun_imagenes_status.iimagen = nurun_imagenes.id');
		$this->db->where('status',$info['option']);
		//$this->db->where('nurun_imagenes_status.iusuario',$this->session->userdata["id"]);
		$q = $this->db->get();
		$data["images"] = $q->result_array();
		$data["error"]=0;
		return $data;
	}
	
	private function buscar_usuario($email){
		$sql="Select id from nurun_usuarios where email = ? and activo=?";
		$query = $this->db->query($sql, array(strtolower($email),1));
		if($query->num_rows()>0){
			#Mail ya registrado mandamos mensaje de error 
			return true;
		}else{
			#No hay registros con este email,procedemos a insertarlo 
			return false;
		}	
	}
	
	
	
}
