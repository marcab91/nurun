<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Nurun extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->model("NurunModel");
	}
	
	public function index(){
		$array_header=array("titulo"=>"Home");
		if($this->session->userdata["logueado"]){
			$usuario = $this->session->all_userdata();
			$array_header["name_user"] =  "¡Bienvenido! ".$usuario["nombre"];
			$array_header["login"] =  false;
			$array_header["upgif"] = true;
		}else{
			$array_header["name_user"] =  "Prueba Nurun";
			$array_header["login"] =  true;
			$array_header["upgif"] = false;
		}
		
		$array_index=$this->NurunModel->load_all_images();
		$array_index["carrusel"]=$this->NurunModel->load_slider();
		$array_index["login"]=($this->session->userdata["logueado"])?true:false;	
		
		$this->load->view('header',$array_header);
		$this->load->view('index',$array_index);
		$this->load->view('footer');
	}
	
	public function login_register(){
		/*Validamos que sea una solicitud vias ajax*/
		if($this->input->is_ajax_request()) {
			$required_name="";
			$accion=1;
			if($this->input->post('accion')==2){
            	$required_name="required";
				$accion=2;
        	}
			
			$fields=array(
					array('field' => 'email',
						  'label' => 'Email',
						  'rules' => 'trim|required|valid_email',
						  'errors' => array(
							'required' => "El campo %s es requerido",
							'valid_email' => "El formato del %s es incorrecto")
						  ),
					array('field' => 'pwd',
						  'label' => 'Password',
						  'rules' => 'trim|required',
						  'errors' => array(
							'required' => "El campo %s es requerido")
						  ),
					array('field' => 'name',
						  'label' => 'Nombre',
						  'rules' => $required_name,
						  'errors' => array(
							'required' => "El campo %s es requerido")
						  )	  
			);
			$this->form_validation->set_rules($fields);
			if($this->form_validation->run()==false){
				$res["error"]=1;
				$res["msg"]=validation_errors();
			}else{
				if($accion==2){
					#Registro
					$res=$this->NurunModel->insert_user($_POST);
				}else if($accion==1){
					#Login
					$res=$this->NurunModel->login_user($_POST);
				}
				
				if($res["error"]==0){
					#Si no hubo algun error iniciamos los datos de session del usuario
					$usuario_data = array(
					   'id' => $res["info"]->id,
					   'nombre' => $res["info"]->nombre,
					   'logueado' => TRUE
           			 );
					 $this->session->set_userdata($usuario_data);
				}
			}
			
			echo json_encode($res);
		}else{
			load_404();
		}
	}
	
	public function destroy_session(){
		/*Validamos que sea una solicitud vias ajax*/
		if($this->input->is_ajax_request()) {
			
			if(!$this->session->userdata["logueado"]){
				$res["error"]=1;
				$res["msg"]="Tu sesión ya ha expirado";
			}else{
				$this->session->sess_destroy();
				$res["error"]=0;
			}
			echo json_encode($res);
			
		}else{
			load_404();
		}
		
	}
	
	public function upfile(){
		/*Validamos que sea una solicitud vias ajax*/
		if($this->input->is_ajax_request()){	
			
			if($this->session->userdata["logueado"]){
				$res=$this->NurunModel->upfile($_FILES);
			}else{
				$res["error"]=1;
				$res["msg"]="Tu sesión ya ha expirado";
			}
			echo json_encode($res);
			
		}else{
			load_404();
		}
	}
	
	public function load_views($view){
		/*Validamos que sea una solicitud vias ajax*/
		if($this->input->is_ajax_request()) {
			if($view==1){
				#Traer vista del Formulario del Login
				$data['view'] = $this->load->view('form_login', NULL, TRUE);	
			}else if($view==2){
				#Traer vista del Formulario para subir una imagen
				$data['view'] = $this->load->view('form_upgif', NULL, TRUE);	
			}
			echo json_encode($data);
		}else{	
			load_404();
		}
	
	}
	
	public function filter_gifs(){
		/*Validamos que sea una solicitud vias ajax*/
		if($this->input->is_ajax_request()) {
			if($this->session->userdata["logueado"]){
				if($_POST["option"]==0){
					$data_view=$this->NurunModel->load_all_images();
					$data_view["good"]='';
					$data_view["no_good"]='';
					$data_view["num_col_md"]=4;
					$data_view["num_col_xs"]=6;
					$data_view["off_set"]='col-md-offset-2';
				}else{
					$data_view=$this->NurunModel->filter_gifs($_POST);
					if($_POST["option"]==1){
						$data_view["good"]='hidden';
						$data_view["no_good"]='';
					}else if($_POST["option"]==2){
						$data_view["good"]='';
						$data_view["no_good"]='hidden';
					}
					$data_view["off_set"]='';
					$data_view["num_col_md"]=12;
					$data_view["num_col_xs"]=12;
				}
				$res["error"]=0;
				$res['view']=$this->load->view('images_ajax',$data_view, TRUE);
			}else{
				$res["error"]=1;
				$res["msg"]="Tu sesión ya ha expirado";
			}
			echo json_encode($res);
		}else{
			load_404();
		}
	}
	
	public function actions_gifs(){
		/*Validamos que sea una solicitud vias ajax*/
		if($this->input->is_ajax_request()){
			if($this->session->userdata["logueado"]){
				$res=$this->NurunModel->add_status_gif($_POST);
			}else{
				$res["error"]=1;
				$res["msg"]="Tu sesión ya ha expirado";
			}
			echo json_encode($res);
		}else{	
			load_404();
		}
	}
	
}