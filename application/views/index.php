
<header id="myCarousel" class="carousel slide">
    <!--Bullets-->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- items -->
    <div id="wrapper_items" class="carousel-inner">
    	<?php
			$cont=0; 
			foreach($carrusel["slider"] as $value){	
			$active=($cont==0)?'active':'';
		?>

            <div class="item <?=$active?> ">
                <a href='<?=$value["ruta"]?>' class="lightbox" data-toggle="lightbox">
                    <div class="fill" style="background-image:url('<?=$value["ruta"]?>');"></div>
                </a>
            </div>
        <?php $cont++; }?>
        
    </div>

    <!-- Controles -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="icon-prev"></span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="icon-next"></span>
    </a>

</header>


<?php if($login){ ?>
<div id="cont_filter" class="container cont_filter">
	<div class"row">
		<div class="col-md-5 col-xs-12">
        	<div class="form-group">
            	<label for="sel1">Filtrar Gifs:</label>
                <select class="form-control" name="attribute_select" id="attribute_select">
                    <option value="0">Todos</option>
                    <option value="1">Aprovados</option>
                    <option value="2">No Aprovados</option> 
                </select>
            </div>
		</div>
	</div>
</div>
<?php }?>

<div class="container">	
	<div id="main_gifs" class="row main_gifs">
    	<?php 
			foreach($images as $value){	
			$url_share=base_url()."".ltrim($value["ruta"],"./");
		?>
          
        	<div id="box_gif_<?=$value["id"]?>" class="col-md-4 col-xs-12 spacing_box_gif">
            	<div class="col-md-12 col-xs-12">
                	<a href='<?=$value["ruta"]?>' class="lightbox" data-toggle="lightbox"><img src="<?=$value["ruta"]?>"  class="gif_alto img-responsive center-block"></a>
                </div>
                <div id="main_actions_gifs" class="col-md-12 col-xs-12 text-center">
                    <div class="box_actions_gifs_<?=$value["id"]?> col-md-4 col-md-offset-2  col-xs-6  ">
                        <div class="text-center"><img type="1" rel="<?=$value["id"]?>" class="actions_gifs img-responsive center-block" src="<?=base_url();?>assets/recursos/good.png"></div>
                    </div>
                    <div class="box_actions_gifs_<?=$value["id"]?> col-md-4 col-xs-6 ">
                        <div class="text-center"><img type="2" rel="<?=$value["id"]?>" class="actions_gifs img-responsive center-block" src="<?=base_url();?>assets/recursos/no_good.png"></div>
                    </div>
					<div class="col-md-12">
						<div class="fb-share-button" data-href="<?=$url_share?>" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse">Compartir</a></div>
					</div>
                </div>
            </div>
         
        <?php }?>
    </div>
</div>


<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

  <!-- Your share button code -->
