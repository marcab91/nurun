<!-- Modal -->
<div id="windows" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="title_modal" class="modal-title">Inicia Sesión</h4>
      </div>
      <div id="modal_body" class="modal-body">
      	<div id="message_modals" class="alert hidden" role="alert"></div>
      </div>
    </div>

  </div>
</div>

<!-- Small Modal -->

<div id="sm_windows" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
      	<button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="title_modal_sm" class="modal-title">Mensaje</h4>
      </div>
      <div id="modal_body" class="modal-body">
      	<div id="message_modals_sm" class="alert hidden" role="alert"></div>
      </div>
    </div>
  </div>
</div>

<!--Font Roboto-->
<link href='https://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'>
<!--Jquery CDN-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Bootstrap CSS CDN -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<!-- Bootstrap JS CDN -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!--Animaciones CSS CDN-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css" type="text/css" rel="stylesheet">
<!--Bootstrap lightbox CSS-->
<link href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.css" type="text/css" rel="stylesheet">
<!--Bootstrap lightbox JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.js"></script>
<!--CSS Slider-->
<link href="<?=base_url();?>assets/slider.css?1.1" type="text/css" rel="stylesheet">
<!--Mis estilos-->
<link href="<?=base_url();?>assets/styles.css?1.9" type="text/css" rel="stylesheet">
<!--Mis funciones JS-->
<script src="<?=base_url();?>assets/js/funciones.js?1.1"></script>
<!--Mis scripts-->
<script src="<?=base_url();?>assets/js/scripts.js?1.2"></script>

</body>
</html>
