<script src="<?=base_url();?>assets/js/ajax_files.js"></script>
<script type="text/javascript">
$(document).ready(function(e) {
   $('#uploadForm').submit(function(e) {	
		if($('#userImage').val()) {
			e.preventDefault();
			$('#loader-icon').show();
			$(this).ajaxSubmit({ 
				dataType:'json',
				target:   '#targetLayer', 
				beforeSubmit: function() {
				  $("#progress-bar").width('0%');
				},
				uploadProgress: function (event, position, total, percentComplete){	
					$("#progress-bar").width(percentComplete + '%');
					$("#progress-bar").html('<div id="progress-status">' + percentComplete +' %</div>')
				},
				success:function (data){
					
					$('#loader-icon').hide();
					show_messages_modals(data.msg,data.error);
					$.ajax({
						url: base_url+'FiltrarGifs',
						type: 'POST',
						dataType: 'json',
						data: {'option' : 0},
						success: function(data){
							if(data.error==0){
								$('#main_gifs').html('');				
								$('#main_gifs').html(data.view);
								 FB.XFBML.parse();
							}
						}
					});
				},
				resetForm: true 
			}); 
			return false; 
		}
	});
 
});
</script>
<div class="box_form">
    <form id="uploadForm" action="<?=base_url();?>subir_gif" method="post">
        <div id="box_input_gif" class="form-group">
        	<i class="glyphicon glyphicon-picture"></i>
        	<label for="gif">Subir Gif:</label>
            <input name="userImage" id="userImage" type="file" class="demoInputBox" />
        </div>
        <div>
	        <input type="submit" id="btnSubmit" value="Subir" class="btn btn-primary" />
        </div>
    	<div id="progress-div">
        	<div id="progress-bar"></div>
        </div>
    	<div id="targetLayer"></div>
    </form>
    <div id="loader-icon" class="text-center" style="display:none;">
    	<img src="<?=base_url();?>assets/recursos/ajax-loader.gif" />
    </div>
</div>