<div class="box_form">
    <form id="form_login">
      <div id="box_input_user" class="form-group hidden">
        <i class="glyphicon glyphicon-user"></i>
        <label for="name">Nombre:</label>
        <input type="text" class="form-control" name="name" id="name">
      </div>   	
      <div class="form-group">
        <i class="glyphicon glyphicon-envelope"></i>
        <label for="email">Email:</label>
        <input type="email" class="form-control" name="email" id="email">
      </div>
      <div class="form-group">
        <i class="glyphicon glyphicon-lock"></i>
        <label for="pwd">Password:</label>
        <input type="password" class="form-control" id="pwd" name="pwd">
      </div>
      <input type="hidden" name="accion" id="accion" value="1">
      <div id="box_btn_forms" class="text-center"><button id="send_form_login" type="submit" class="btn btn-primary">Entrar</button></div>
      
      <hr />
      <a class="change_form" rel="1"><h4 id="txt_link_form" class="text-center">Quiero Registrarme</h4></a>
    </form>
</div>