var class_messages = ["alert-success","alert-danger"];

function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}


/*Funcion para mostrar los mensajes en los modals*/
function show_messages_modals(txt,accion){
	$("#message_modals").html(txt);
	$("#message_modals").removeClass("hidden");
	$("#message_modals").addClass("animated rubberBand");
	$("#message_modals").addClass(class_messages[accion]);
	
	setTimeout(function() { 
		$("#message_modals").addClass("hidden");
		$("#message_modals").removeClass(class_messages[accion]);				
				
	}, 5000);	
}

/*Funciones spineer*/
function cargando(elemento,ocultar){
	$(".loader_forms").remove();
	$(ocultar).hide();
	var imagen=base_url+"assets/recursos/ajax-loader.gif";
	$(elemento).append("<img class='loader_forms' style='height:35px; width:35px;' src='"+imagen+"'>");
}

function hide_spinner(ocultar){
	$(".loader_forms").remove();
	$(ocultar).show();
}