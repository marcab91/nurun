/*Script para activar el slider del home*/
$('.carousel').carousel({
   interval: 5000
});

/*Lightbox*/
$(document).on('click', '[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', function(event) {
	event.preventDefault();
	return $(this).ekkoLightbox({
		onShown: function() {
			
		},
		onNavigate: function(direction, itemIndex) {
			
		}
	});
});

/*Control de visibilidad de los forms Login/Registro*/
$(document).on("click",".change_form",function(event){
	event.preventDefault();
	var rel = $(this).attr("rel");
	if(rel==1){
		$("#title_modal").text('Registrarme');
		$("#box_input_user").removeClass("hidden");
		$("#box_input_user").addClass('animated bounceIn');
		$(this).attr("rel","2")
		$("#txt_link_form").text('Ya tengo cuenta');
		$("#accion").val(2);
		
	}else if(rel==2){
		
		$("#title_modal").text('Iniciar Sesión');
		$('#box_input_user').addClass('hidden');
		$(this).attr("rel","1")
		$("#txt_link_form").text('Quiero Registrarme');
		$("#accion").val(1);
	}
});

/*Validación y envio del formualrio Login/Register*/
$(document).on("click","#send_form_login",function(event){
	event.preventDefault();
	var pasa=true;
	if(!isEmail($("#email").val())){
		show_messages_modals('Correo No Valido',1);
		pasa=false;
		return false;	
	}
	
	if($("#pwd").val()===''){
		show_messages_modals('Ingresa tu contraseña',1);
		pasa=false;
		return false;
	}
	
	if($("#accion").val()==2){
		if($("#name").val()===''){
			show_messages_modals('Ingresa un nombre',1);
			pasa=false;
			return false;
		}
	}
	
	if(pasa){
		$.ajax({
			type:"POST",
			url:base_url+"firma_usuario",
			dataType:"json", 
			beforeSend: function(){
				cargando("#box_btn_forms","#send_form_login");
			},
			data:$("#form_login").serialize(),
			success: function(data){
				hide_spinner("#send_form_login");
				show_messages_modals(data.msg,data.error);
				if(data.error==0){
					setTimeout(function() { 
						location.reload();
					}, 2000);	
				}
			}
    	});	
	}
});

$(document).ready(function(e){
	/*Mostrar ventana para control de acceso del usurio*/
	$("#sign").click(function(event) {
        event.preventDefault();
		$('#windows').modal('toggle');
		$.ajax({
			type:"POST",
			url:base_url+"ventanas/1",
			dataType:"json", 
			success: function(data){
				$("#title_modal").text('Iniciar Sesión');
				$(".box_form").remove();
				$("#modal_body").append(data.view);
			}
    	});	
    });
	
	/*Mostrar ventana para subir gifs del usuario*/
	$("#upgif").click(function(e) {
        event.preventDefault();
		$('#windows').modal('toggle');
		$.ajax({
			type:"POST",
			url:base_url+"ventanas/2",
			dataType:"json", 
			success: function(data){
				$("#title_modal").text('Subir Gif');
				$(".box_form").remove();
				$("#modal_body").append(data.view);
			}
    	});	
    });
	
	/*Logout del usuario*/
	$("#salir").click(function(e) {
        event.preventDefault();
		$.ajax({
			type:"POST",
			url:base_url+"salir",
			dataType:"json", 
			success: function(data){
				if(data.error==0){
					location.reload();
				}else{
					alert(data.msg);
				}
			}
    	});	

    });
	
	
	
});